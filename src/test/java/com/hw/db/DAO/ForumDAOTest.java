package com.hw.db.DAO;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import com.hw.db.models.Forum;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

@SuppressWarnings("unchecked")
class ForumDAOTest {

    public static final JdbcTemplate JDBC_TEMPLATE = mock(JdbcTemplate.class);

    public static final MockedStatic<ForumDAO> FORUM_DAO_MOCKED_STATIC = mockStatic(ForumDAO.class);

    @BeforeAll
    static void setup() {
        new ForumDAO(JDBC_TEMPLATE);
    }

    @BeforeEach
    void setupEach() {
        Mockito.reset(JDBC_TEMPLATE);
    }

    @Test
    void forumTest() {
        Forum forum = new Forum();
        forum.setSlug("slug");
        forum.setTitle("title");
        forum.setUser("user");

        ForumDAO.CreateForum(forum);

        FORUM_DAO_MOCKED_STATIC.verify(times(1), () -> ForumDAO.CreateForum(forum));
    }

    @Test
    void createThreadTest() {
        Thread thread = new Thread();
        Thread thread2 = new Thread();
        User user = new User();

        when(JDBC_TEMPLATE.queryForObject(
            any(),
            any(RowMapper.class),
            any(),
            any(),
            any(),
            any(),
            any(),
            any()
        )).thenReturn(thread2);

        FORUM_DAO_MOCKED_STATIC
            .when(() -> ForumDAO.CreateThread(thread, user))
            .thenCallRealMethod();

        Thread resultThread = ForumDAO.CreateThread(thread, user);

        assertEquals(thread2, resultThread);
    }

    @Test
    void createThreadFilledCreatedTest() {
        Thread thread = new Thread();
        thread.setCreated(Timestamp.from(Instant.now()));
        Thread thread2 = new Thread();
        User user = new User();

        when(JDBC_TEMPLATE.queryForObject(
            any(),
            any(RowMapper.class),
            any(),
            any(),
            any(),
            any(),
            any(),
            any()
        )).thenReturn(thread2);

        FORUM_DAO_MOCKED_STATIC
            .when(() -> ForumDAO.CreateThread(thread, user))
            .thenCallRealMethod();

        Thread resultThread = ForumDAO.CreateThread(thread, user);

        assertEquals(thread2, resultThread);
    }

    @Test
    void createThreadDuplicateExceptionTest() {
        ByteArrayOutputStream captor = new ByteArrayOutputStream();
        System.setOut(new PrintStream(captor));

        Thread thread = new Thread();
        thread.setCreated(Timestamp.from(Instant.now()));
        Thread thread2 = new Thread();
        User user = new User();

        when(JDBC_TEMPLATE.queryForObject(
            any(),
            any(RowMapper.class),
            any(),
            any(),
            any(),
            any(),
            any(),
            any()
        )).thenReturn(thread2);

        when(JDBC_TEMPLATE.update(
            any(),
            any(),
            any(),
            any(),
            any(),
            any()
        )).thenThrow(new DuplicateKeyException("AAAAA"));

        FORUM_DAO_MOCKED_STATIC
            .when(() -> ForumDAO.CreateThread(thread, user))
            .thenCallRealMethod();

        Thread resultThread = ForumDAO.CreateThread(thread, user);

        assertEquals(thread2, resultThread);
        assertEquals("Already exists;", captor.toString().trim());
    }

    @Test
    void infoTest() {
        Forum forum = new Forum();
        forum.setSlug("slug");

        when(JDBC_TEMPLATE.queryForObject(
            any(),
            any(RowMapper.class),
            any()
        )).thenReturn(forum);

        FORUM_DAO_MOCKED_STATIC
            .when(() -> ForumDAO.Info("slug"))
            .thenCallRealMethod();

        assertEquals(forum, ForumDAO.Info("slug"));
    }

    @Test
    void bTest() {
        List<User> userList = Collections.singletonList(new User());

        when(JDBC_TEMPLATE.query(
            any(),
            any(Object[].class),
            any(RowMapper.class)
        )).thenReturn(userList);

        FORUM_DAO_MOCKED_STATIC
            .when(() -> ForumDAO.UserList(any(), any(), any(), any()))
            .thenCallRealMethod();

        assertEquals(userList, ForumDAO.UserList("slug", null, "null", null));
    }

    @Test
    void cTest() {
        List<User> userList = Collections.singletonList(new User());

        when(JDBC_TEMPLATE.query(
            any(),
            any(Object[].class),
            any(RowMapper.class)
        )).thenReturn(userList);

        FORUM_DAO_MOCKED_STATIC
            .when(() -> ForumDAO.UserList(any(), any(), any(), any()))
            .thenCallRealMethod();

        assertEquals(userList, ForumDAO.UserList("slug", null, "null", true));
    }

    @Test
    void dTest() {
        List<User> userList = Collections.singletonList(new User());

        when(JDBC_TEMPLATE.query(
            any(),
            any(Object[].class),
            any(RowMapper.class)
        )).thenReturn(userList);

        FORUM_DAO_MOCKED_STATIC
            .when(() -> ForumDAO.UserList(any(), any(), any(), any()))
            .thenCallRealMethod();

        assertEquals(userList, ForumDAO.UserList("slug", 1, "null", true));
    }

}