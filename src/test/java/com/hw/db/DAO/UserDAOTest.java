package com.hw.db.DAO;

import static org.mockito.Mockito.mock;

import com.hw.db.models.User;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.core.JdbcTemplate;

class UserDAOTest {

    public static final JdbcTemplate JDBC_TEMPLATE = mock(JdbcTemplate.class);

    @BeforeAll
    static void setup() {
        new UserDAO(JDBC_TEMPLATE);
    }

    @Test
    void aTest() {
        User user = new User();
        user.setAbout("a");

        UserDAO.Change(user);
    }

    @Test
    void bTest() {
        User user = new User();
        user.setEmail(null);
        user.setNickname(null);
        user.setFullname("a");
        user.setAbout("a");

        UserDAO.Change(user);
    }

    @Test
    void cTest() {
        User user = new User();
        user.setEmail(null);
        user.setNickname("a");
        user.setFullname(null);
        user.setAbout("a");

        UserDAO.Change(user);
    }

    @Test
    void dTest() {
        User user = new User();
        user.setEmail("a");
        user.setNickname(null);
        user.setFullname(null);
        user.setAbout("a");

        UserDAO.Change(user);
    }

    @Test
    void eTest() {
        User user = new User();
        user.setEmail("a");
        user.setNickname(null);
        user.setFullname("a");
        user.setAbout("a");

        UserDAO.Change(user);
    }

    @Test
    void fTest() {
        User user = new User();
        user.setEmail("a");
        user.setNickname("a");
        user.setFullname(null);
        user.setAbout("a");

        UserDAO.Change(user);
    }

    @Test
    void gTest() {
        User user = new User();
        user.setEmail("a");
        user.setNickname("a");
        user.setFullname("a");
        user.setAbout("a");

        UserDAO.Change(user);
    }

    @Test
    void hTest() {
        User user = new User();
        user.setEmail(null);
        user.setNickname("a");
        user.setFullname("a");
        user.setAbout("a");

        UserDAO.Change(user);
    }
}