package com.hw.db.DAO;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.hw.db.models.Post;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

class PostDAOTest {

    public static final JdbcTemplate JDBC_TEMPLATE = mock(JdbcTemplate.class);

    @BeforeAll
    static void setup() {
        new PostDAO(JDBC_TEMPLATE);
    }

    @BeforeEach
    void setupEach() {
        Mockito.reset(JDBC_TEMPLATE);
    }

    @Test
    void aTest() {
        Post post = new Post();
        post.setAuthor(null);
        post.setMessage(null);
        post.setCreated(Timestamp.from(Instant.now()));

        Post post2 = new Post();
        post2.setAuthor(null);
        post2.setMessage(null);
        post2.setCreated(Timestamp.from(Instant.now().plus(1, ChronoUnit.DAYS)));

        when(JDBC_TEMPLATE.queryForObject(any(), any(RowMapper.class), any())).thenReturn(post2);

        PostDAO.setPost(1, post);
    }

    @Test
    void bTest() {
        Post post = new Post();
        post.setAuthor(null);
        post.setMessage("null");
        post.setCreated(null);

        Post post2 = new Post();
        post2.setAuthor(null);
        post2.setMessage("null1");
        post2.setCreated(null);

        when(JDBC_TEMPLATE.queryForObject(any(), any(RowMapper.class), any())).thenReturn(post2);

        PostDAO.setPost(1, post);
    }

    @Test
    void cTest() {
        Post post = new Post();
        post.setAuthor(null);
        post.setMessage("null");
        post.setCreated(Timestamp.from(Instant.now()));

        Post post2 = new Post();
        post2.setAuthor(null);
        post2.setMessage("null1");
        post2.setCreated(Timestamp.from(Instant.now().plus(1, ChronoUnit.DAYS)));

        when(JDBC_TEMPLATE.queryForObject(any(), any(RowMapper.class), any())).thenReturn(post2);

        PostDAO.setPost(1, post);
    }

    @Test
    void dTest() {
        Post post = new Post();
        post.setAuthor("null");
        post.setMessage(null);
        post.setCreated(null);

        Post post2 = new Post();
        post2.setAuthor("null1");
        post2.setMessage(null);
        post2.setCreated(null);

        when(JDBC_TEMPLATE.queryForObject(any(), any(RowMapper.class), any())).thenReturn(post2);

        PostDAO.setPost(1, post);
    }

    @Test
    void eTest() {
        Post post = new Post();
        post.setAuthor("null");
        post.setMessage(null);
        post.setCreated(Timestamp.from(Instant.now()));

        Post post2 = new Post();
        post2.setAuthor("null1");
        post2.setMessage(null);
        post2.setCreated(Timestamp.from(Instant.now().plus(1, ChronoUnit.DAYS)));

        when(JDBC_TEMPLATE.queryForObject(any(), any(RowMapper.class), any())).thenReturn(post2);

        PostDAO.setPost(1, post);
    }

    @Test
    void fTest() {
        Post post = new Post();
        post.setAuthor("null");
        post.setMessage("null");
        post.setCreated(null);

        Post post2 = new Post();
        post2.setAuthor("null1");
        post2.setMessage("null1");
        post2.setCreated(null);

        when(JDBC_TEMPLATE.queryForObject(any(), any(RowMapper.class), any())).thenReturn(post2);

        PostDAO.setPost(1, post);
    }

    @Test
    void gTest() {
        Post post = new Post();
        post.setAuthor("null");
        post.setMessage("null");
        post.setCreated(Timestamp.from(Instant.now()));

        Post post2 = new Post();
        post2.setAuthor("null1");
        post2.setMessage("null1");
        post2.setCreated(Timestamp.from(Instant.now().plus(1, ChronoUnit.DAYS)));

        when(JDBC_TEMPLATE.queryForObject(any(), any(RowMapper.class), any())).thenReturn(post2);

        PostDAO.setPost(1, post);
    }

    @Test
    void hTest() {
        Post post = new Post();
        post.setAuthor(null);
        post.setMessage(null);
        post.setCreated(null);

        Post post2 = new Post();
        post2.setAuthor(null);
        post2.setMessage(null);
        post2.setCreated(null);

        when(JDBC_TEMPLATE.queryForObject(any(), any(RowMapper.class), any())).thenReturn(post2);

        PostDAO.setPost(1, post);
    }
}