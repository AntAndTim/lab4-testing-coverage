package com.hw.db.DAO;

import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.hw.db.models.Post;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.List;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

class ThreadDAOTest {

    public static final JdbcTemplate JDBC_TEMPLATE = mock(JdbcTemplate.class);
    public static final List<Post> POSTS = singletonList(new Post());

    @BeforeAll
    static void setUpAll() {
        new ThreadDAO(JDBC_TEMPLATE);
    }

    @BeforeEach
    void setUp() {
        Mockito.reset(JDBC_TEMPLATE);

        when(JDBC_TEMPLATE.query(
            any(),
            any(RowMapper.class),
            any()
        )).thenReturn(POSTS);
    }

    @Test
    void aTest() {
        ByteArrayOutputStream captor = new ByteArrayOutputStream();
        System.setOut(new PrintStream(captor));

        assertEquals(POSTS, ThreadDAO.treeSort(1, null, 1, false));
        assertEquals("2", captor.toString().trim());
    }

    @Test
    void bTest() {
        ByteArrayOutputStream captor = new ByteArrayOutputStream();
        System.setOut(new PrintStream(captor));

        assertEquals(POSTS, ThreadDAO.treeSort(1, 1, 1, true));
        assertEquals("2", captor.toString().trim());
    }
}